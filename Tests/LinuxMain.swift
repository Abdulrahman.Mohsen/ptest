import XCTest

import PTestTests

var tests = [XCTestCaseEntry]()
tests += PTestTests.allTests()
XCTMain(tests)
