import XCTest
@testable import PTest

final class PTestTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(PTest().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
