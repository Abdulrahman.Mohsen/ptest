struct PTest {
    var text = "Hello, World!"
}

public struct stack<Element> {
    private var storage = [Element]()
    
    public init(){}
  public  func peek()->Element?{
        return storage.last
    }
    
    mutating public func push(model : Element){
        storage.append(model)
    }
    
    mutating public func pop()-> Element?{
       return storage.popLast()
    }
    
}
